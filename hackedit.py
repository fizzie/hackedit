#! /usr/bin/env python3

import os.path
import re
import requests
import requests.exceptions
import urllib.parse

from flask import Flask, Response, render_template, request, url_for
app = Flask(__name__)

# constants

HACKEGO_FSHG = 'http://hackego.esolangs.org/fshg/index.cgi/raw-file/tip/{target}'
HACKEGO_TMP = 'http://hackego.esolangs.org/tmp/{target}'
HACKEGO_TIMEOUT = 5.0

MAX_FILE_SIZE = 16*1024*1024

# routes

@app.route('/get/<path:target>')
def get(target):
    local_copy = storage_path(target)
    if not os.path.isfile(local_copy):
        return 'Not found: ' + target, 404, { 'Content-Type': 'text/plain' }

    try:
        with open(local_copy, 'r') as f:
            return f.read(), 200, {
                'Content-Type': 'text/plain',
                'X-Content-Type-Options': 'nosniff',
                'X-Robots-Tag': 'nofollow',
            }
    except OSError as e:
        return 'Unable to read: {}: {}'.format(target, e), 500, { 'Content-Type': 'text/plain' }

@app.route('/edit/')
def edit_index():
    files = [
        dict(name = path,
             edit_url = url_for('edit', target=path))
        for path in storage_files()
    ]

    return render_template(
        'index.html',
        files=files)

@app.route('/edit/<path:target>', methods=['POST', 'GET'])
def edit(target):
    contents = ''
    errors = []

    if 'sync' in request.form:
        try:
            contents = hackego_fetch(target)
            save(target, contents)
        except requests.exceptions.RequestException as e:
            errors.append('Fetching from HackEgo failed: ' + str(e))
    elif 'save' in request.form:
        contents = request.form['contents']
        save(target, contents)

    if not contents:
        try:
            with open(storage_path(target), 'r') as f:
                contents = f.read(MAX_FILE_SIZE)
        except OSError:
            pass  # I guess we didn't have it

    if not contents.endswith('\n'):
        errors.append('Warning: no trailing newline in saved file.')

    get_url = url_for('get', target=target, _external=True)
    if ' ' in target:
        fetch_command = '`fetch {}'.format(get_url)
    else:
        fetch_command = '`fetch {} {}'.format(target, get_url)

    return render_template(
        'edit.html',
        target = target,
        edit_url = url_for('edit', target=target),
        get_url = get_url,
        fetch_command = fetch_command,
        contents = contents,
        errors = errors)

# utilities

def hackego_fetch(target):
    target = urllib.parse.quote(target)
    if target.startswith('tmp/'):
        target_url = HACKEGO_TMP.format(target=target[4:])
    else:
        target_url = HACKEGO_FSHG.format(target=target)

    r = requests.get(target_url)
    r.raise_for_status()
    return r.text

def save(target, contents):
    try:
        with open(storage_path(target), 'w') as f:
            f.write(contents)
    except OSError:
        pass  # let's just don't and say that we did

def encode_path(target):
    '''Encode a path name to a form usable as a file name.'''

    def encode_char(match):
        char = match.group(0)
        assert len(char) == 1
        if char == '/':
            return '-'
        else:
            return '+{0:x}+'.format(ord(char))

    return re.sub(r'[^a-zA-Z0-9._ ]', encode_char, target)

def decode_path(file_name):
    '''Inverse of encode_path.'''

    def decode_char(match):
        return chr(int(match.group(1), 16))

    if not re.match(r'^([a-zA-Z0-9._ -]|\+[0-9a-f]+\+)+$', file_name):
        return None
    return re.sub(r'\+([0-9a-f]+)\+', decode_char, file_name).replace('-', '/')

def storage_path(target):
    '''Returns local path for a particular HackEgo repository target.'''

    return os.path.join(app.instance_path, encode_path(target))

def storage_files():
    '''Returns the list of targets that there are local versions of.'''

    files = [path for path
             in (decode_path(f) for f in os.listdir(app.instance_path))
             if path is not None]
    files.sort()
    return files

# for development use only

if __name__ == '__main__':
    app.run()
